package com.cbsystematics.homeworks.hw05.taskExtra;

import com.cbsystematics.homeworks.hw05.taskExtra.exceptions.EmptyDataException;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class MainTest {
    
    @Test
    public void getTeachersList_TestingForEquality_ShouldBeEqual() throws EmptyDataException {
        String[] testArray = {"String1", "String2", "String3"};
        String[] expectedTrue = {"String1 teacher", "String2 teacher", "String3 teacher"};
    
        Assert.assertArrayEquals(Main.getTeachersList(testArray).toArray(), expectedTrue);
    }
    
    @Test
    public void getTeachersList_TestingForEquality_ShouldBeNotEqual() throws EmptyDataException {
        String[] testArray = {"String1", "String2", "String3"};
        String[] expectedFalse = {"String1", "String2", "String3"};
    
        Assert.assertFalse(Arrays.equals(Main.getTeachersList(testArray).toArray(), expectedFalse));
    }
    
    @Test
    public void getTeachersList_TestingForException_ShouldCatchException() {
        try {
            Main.getTeachersList(new String[0]);
            Assert.fail("Expected exception");
        } catch (EmptyDataException e) {
            Assert.assertEquals("-> Array of subjects is empty!", e.getMessage());
        }
    }
    
    @Test(expected = EmptyDataException.class)
    public void getTeachersList_TestingForException_ShouldAnnotateException() throws EmptyDataException {
        Main.getTeachersList(new String[0]);
    }
    
    @Test
    public void getFormattedData_TestingForException_ShouldCatchException() {
        try {
            Main.getFormattedData(new ArrayList<>());
            Assert.fail("Expected exception");
        } catch (EmptyDataException e) {
            Assert.assertEquals("-> List of teachers is empty!", e.getMessage());
        }
    }
}
