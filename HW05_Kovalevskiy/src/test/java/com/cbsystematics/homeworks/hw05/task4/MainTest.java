package com.cbsystematics.homeworks.hw05.task4;

import com.cbsystematics.homeworks.hw05.task4.exceptions.EmptyDataException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class MainTest {
    
    @Test
    public void getArrayListByIterator_TestingForEquality_ShouldBeEqual() throws EmptyDataException {
        Integer[] testArray = {1, 2, 3, 4, 5};
        Integer[] expectedTrue = {2, 3, 4, 5, 6};
    
        Assert.assertArrayEquals(Main.getArrayListByIterator(testArray).toArray(), expectedTrue);
    }
    
    @Test
    public void getArrayListByIterator_TestingForEquality_ShouldBeNotEqual() throws EmptyDataException {
        Integer[] testArray = {1, 2, 3, 4, 5};
        Integer[] expectedFalse = {1, 2, 3, 4, 5};
    
        Assert.assertFalse(Arrays.equals(Main.getArrayListByIterator(testArray).toArray(), expectedFalse));
    }
    
    @Test
    public void getTeachersList_TestingForException_ShouldCatchException() {
        try {
            com.cbsystematics.homeworks.hw05.task4.Main.getArrayListByIterator(new Integer[0]);
            Assert.fail("Expected exception");
        } catch (EmptyDataException e) {
            Assert.assertEquals("-> Array of numbers is empty!", e.getMessage());
        }
    }
    
    /*@Test(expected = EmptyDataException.class)
    public void getTeachersList_TestingForException_ShouldAnnotateException() throws EmptyDataException {
        com.cbsystematics.homeworks.hw05.task4.Main.getArrayListByIterator(new Integer[0]);
    }*/
    
    @Test
    public void getArrayListByReplace_TestingForEquality_ShouldBeEqual() throws EmptyDataException {
        Integer[] testArray = {1, 2, 3, 4, 5};
        Integer[] expectedTrue = {2, 3, 4, 5, 6};
        
        Assert.assertArrayEquals(Main.getArrayListByReplace(testArray).toArray(), expectedTrue);
    }
    
    @Test
    public void getArrayListByReplace_TestingForEquality_ShouldBeNotEqual() throws EmptyDataException {
        Integer[] testArray = {1, 2, 3, 4, 5};
        Integer[] expectedFalse = {1, 2, 3, 4, 5};
        
        Assert.assertFalse(Arrays.equals(Main.getArrayListByReplace(testArray).toArray(), expectedFalse));
    }
    
    @Test
    public void getArrayListByReplace_TestingForException_ShouldCatchException() {
        try {
            com.cbsystematics.homeworks.hw05.task4.Main.getArrayListByReplace(new Integer[0]);
            Assert.fail("Expected exception");
        } catch (EmptyDataException e) {
            Assert.assertEquals("-> Array of numbers is empty!", e.getMessage());
        }
    }
    
    /*@Test(expected = EmptyDataException.class)
    public void getArrayListByReplace_TestingForException_ShouldAnnotateException() throws EmptyDataException {
        com.cbsystematics.homeworks.hw05.task4.Main.getArrayListByReplace(new Integer[0]);
    }*/
    
    @Test
    public void getArrayListByForeach_TestingForEquality_ShouldBeEqual() throws EmptyDataException {
        Integer[] testArray = {1, 2, 3, 4, 5};
        Integer[] expectedTrue = {2, 3, 4, 5, 6};
        
        Assert.assertArrayEquals(Main.getArrayListByForeach(testArray).toArray(), expectedTrue);
    }
    
    @Test
    public void getArrayListByForeach_TestingForEquality_ShouldBeNotEqual() throws EmptyDataException {
        Integer[] testArray = {1, 2, 3, 4, 5};
        Integer[] expectedFalse = {1, 2, 3, 4, 5};
        
        Assert.assertFalse(Arrays.equals(Main.getArrayListByForeach(testArray).toArray(), expectedFalse));
    }
    
    @Test
    public void getArrayListByForeach_TestingForException_ShouldCatchException() {
        try {
            com.cbsystematics.homeworks.hw05.task4.Main.getArrayListByForeach(new Integer[0]);
            Assert.fail("Expected exception");
        } catch (EmptyDataException e) {
            Assert.assertEquals("-> Array of numbers is empty!", e.getMessage());
        }
    }
    
    /*@Test(expected = EmptyDataException.class)
    public void getArrayListByForeach_TestingForException_ShouldAnnotateException() throws EmptyDataException {
        com.cbsystematics.homeworks.hw05.task4.Main.getArrayListByForeach(new Integer[0]);
    }*/
}
