package com.cbsystematics.homeworks.hw05.task2;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Main Class presents homework on ArrayList manipulations
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Zoo {
    
    public static void main(String[] args) {
        ArrayList<String> animals = Zoo.getAnimalsList();
    
        AtomicInteger index = new AtomicInteger(1);

        animals.replaceAll(item -> index.getAndIncrement() + ". " + item);
        animals.forEach(System.out::println);
    }
    
    /**
     * The method create, fill and return an ArrayList
     *
     * @return ArrayList
     */
    public static ArrayList<String> getAnimalsList() {
        ArrayList<String> animals = new ArrayList<>();
        animals.add("animal1");
        animals.add(1, "animal2");
        animals.add(2, "animal3");
        animals.add(3, "animal4");
        animals.add(4, "animal5");
        animals.add("animal6");
        animals.add("animal7");
        animals.add(7, "animal8");
        
        return animals;
    }
}
