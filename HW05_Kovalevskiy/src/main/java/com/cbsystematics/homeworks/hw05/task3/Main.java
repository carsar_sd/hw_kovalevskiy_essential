package com.cbsystematics.homeworks.hw05.task3;

import com.cbsystematics.homeworks.hw05.task2.Zoo;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Main Class presents homework on ArrayList manipulations
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Main {
    
    public static void main(String[] args) {
        ArrayList<String> animals = Zoo.getAnimalsList();
    
        System.out.println("\nold List: " + Arrays.toString(animals.toArray()));
        System.out.println("old Size: " + animals.size());
        System.out.println("---------");
        
        animals.remove(6);
        animals.remove(4);
        animals.remove(2);
        
        System.out.println("new List: " + Arrays.toString(animals.toArray()));
        System.out.println("new Size: " + animals.size() + "\n");
    }
}
