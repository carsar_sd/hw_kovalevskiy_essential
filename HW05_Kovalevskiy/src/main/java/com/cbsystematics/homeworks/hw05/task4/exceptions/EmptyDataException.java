package com.cbsystematics.homeworks.hw05.task4.exceptions;

public class EmptyDataException extends Exception {
    
    public EmptyDataException(String errorMessage) {
        super(errorMessage);
    }
}
