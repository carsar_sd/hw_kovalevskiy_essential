package com.cbsystematics.homeworks.hw05.taskExtra.exceptions;

public class EmptyDataException extends Exception {
    
    public EmptyDataException(String errorMessage) {
        super(errorMessage);
    }
}
