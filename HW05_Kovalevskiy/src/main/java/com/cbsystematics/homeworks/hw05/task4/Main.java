package com.cbsystematics.homeworks.hw05.task4;

import com.cbsystematics.homeworks.hw05.task4.exceptions.EmptyDataException;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Main Class presents homework on ArrayList manipulations
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Main {
    
    public static void main(String[] args) throws EmptyDataException {
        Integer[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        
        ArrayList<Integer> arrayList1 = Main.getArrayListByIterator(array);
        System.out.println(Arrays.toString(arrayList1.toArray()));
        
        System.out.println("---");
        
        ArrayList<Integer> arrayList2 = Main.getArrayListByReplace(array);
        System.out.println(Arrays.toString(arrayList2.toArray()));
    
        System.out.println("---");
    
        ArrayList<Integer> arrayList3 = Main.getArrayListByForeach(array);
        System.out.println(Arrays.toString(arrayList3.toArray()));
    }
    
    /**
     * The method converts an Integer array to an ArrayList with minor modifications
     *
     * @param array Integer array
     *
     * @return modified ArrayList
     */
    public static ArrayList<Integer> getArrayListByIterator(Integer[] array) throws EmptyDataException {
        if (array.length == 0) {
            throw new EmptyDataException("-> Array of numbers is empty!");
        }
        
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(array));
    
        ListIterator<Integer> listIterator = arrayList.listIterator();
    
        while (listIterator.hasNext()) {
            int value = listIterator.next();
            int index = listIterator.previousIndex();
            arrayList.set(index, value + 1);
        }
        
        return arrayList;
    }
    
    /**
     * The method converts an Integer array to an ArrayList with minor modifications
     *
     * @param array Integer array
     *
     * @return modified ArrayList
     */
    public static ArrayList<Integer> getArrayListByReplace(Integer[] array) throws EmptyDataException {
        if (array.length == 0) {
            throw new EmptyDataException("-> Array of numbers is empty!");
        }
        
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(array));
    
        arrayList.replaceAll(item -> item + 1);
    
        return arrayList;
    }
    
    /**
     * The method converts an Integer array to an ArrayList with minor modifications
     *
     * @param array Integer array
     *
     * @return modified ArrayList
     */
    public static ArrayList<Integer> getArrayListByForeach(Integer[] array) throws EmptyDataException {
        if (array.length == 0) {
            throw new EmptyDataException("-> Array of numbers is empty!");
        }
        
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(array));
        
        IntStream.range(0, arrayList.size()).forEach(item -> arrayList.set(item, arrayList.get(item) + 1));
        
        return arrayList;
    }
}

/*
    - Tests exists! - нужны комментарии, если будут
*/