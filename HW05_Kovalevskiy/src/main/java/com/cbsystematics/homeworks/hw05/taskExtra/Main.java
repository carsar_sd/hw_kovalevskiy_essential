package com.cbsystematics.homeworks.hw05.taskExtra;

import com.cbsystematics.homeworks.hw05.taskExtra.exceptions.EmptyDataException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Main Class presents homework on ArrayList manipulations
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Main {
    
    private static final Random random = new Random();

    public static void main(String[] args) throws EmptyDataException {
        String[] subjects = {
                "Economics", "Biology", "Chemistry", "Physics", "Geography", "History", "Mathematics", "Literature",
                "French", "German", "Spanish", "Physical education", "Computer applications", "Psychology"
        };
    
        ArrayList<String> teachersList = Main.getTeachersList(subjects);
        String teachersData = Main.getFormattedData(teachersList);
        
        System.out.println(teachersData);
    }

    /**
     * The method converts a string array to an ArrayList with minor modifications
     *
     * @param subjects string array
     *
     * @return modified ArrayList
     */
    public static ArrayList<String> getTeachersList(String[] subjects) throws EmptyDataException {
        if (subjects.length == 0) {
            throw new EmptyDataException("-> Array of subjects is empty!");
        }
        
        ArrayList<String> teachers = new ArrayList<>(Arrays.asList(subjects));
        teachers.replaceAll(item -> item.concat(" teacher"));
        
        return teachers;
    }
    
    /**
     * The method takes an ArrayList finds data of interest and returns it as a formatted String
     *
     * @param teachersList ArrayList
     *
     * @return formatted String
     */
    public static String getFormattedData(ArrayList<String> teachersList) throws EmptyDataException {
        if (teachersList.isEmpty()) {
            throw new EmptyDataException("-> List of teachers is empty!");
        }
        
        int teachersListSize = teachersList.size();
        int randBestIndex = Main.random.nextInt(teachersListSize);
        int randWorseIndex = Main.random.nextInt(teachersListSize);
    
        String teachers = teachersList.toString()
                .replaceAll("((?:[^,]*,){4}[^,]*),", "$1,\n ")
                .replaceAll("[\\[\\]]", "");
    
        return String.format("\nTeachers: \n  %s\nBest : %s\nWorse: %s\n",
                teachers,
                teachersList.get(randBestIndex),
                teachersList.get(randWorseIndex));
    }
}

/*
    - Tests exists! - нужны комментарии, если будут
*/