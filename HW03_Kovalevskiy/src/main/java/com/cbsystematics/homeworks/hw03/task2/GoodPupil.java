package com.cbsystematics.homeworks.hw03.task2;

/**
 * GoodPupil Class is an inheritor of Pupil class.
 * Has overridden methods that outputs a String information of current Pupil.
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class GoodPupil extends Pupil {
    
    public GoodPupil(String pupilName) {
        super(pupilName);
    }
    
    @Override
    public String study() {
        return "good level of pupil achievement";
    }
    
    @Override
    public String read() {
        return "good level of pupil reading";
    }
    
    @Override
    public String write() {
        return "good level of pupil writing";
    }
    
    @Override
    public String relax() {
        return "good level of pupil relaxing";
    }
}
