package com.cbsystematics.homeworks.hw03.task3;

/**
 * Car Class is an inheritor of Vehicle class.
 * Describes Car object characteristics
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Car extends Vehicle {

    public Car(String coords, int price, int speed, int year) {
        super(coords, price, speed, year);
    }

    /**
     * The method print information about the Car
     */
    @Override
    public void printInformation() {
        System.out.println(
                "Car   \uD83D\uDE97 [ " +
                "coords: " + this.coords +
                ", price: " + String.format("%,d", this.price).replace(",", " ") + " USD" +
                ", speed: " + this.speed + " km/h" +
                ", year: " + this.year +
                " ]"
        );
    }
}
