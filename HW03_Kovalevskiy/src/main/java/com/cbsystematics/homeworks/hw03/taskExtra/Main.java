package com.cbsystematics.homeworks.hw03.taskExtra;

/**
 * Main Class is a main class that shows the result of using Printer classes
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Main {

    public static void main(String[] args) {
        Printer printerBlue = new PrinterBlue();
        printerBlue.print("It must be blue color text!");

        Printer printerRed = new PrinterRed();
        printerRed.print("It must be red color text!");

        Printer printerGreen = new PrinterGreen();
        printerGreen.print("It must be green color text!");
    }
}

/*
 - не понял на счет "Обязательно используйте приведение типов"
*/