package com.cbsystematics.homeworks.hw03.task2;

/**
 * Pupil Class is a parent of the pupil class group.
 * Has getter and methods that outputs a String information.
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Pupil {
    
    String pupilName;
    
    public Pupil(String pupilName) {
        this.pupilName = pupilName;
    }
    
    public String getPupilName() {
        return pupilName;
    }
    
    public String study() {
        return "basic level of pupil achievement";
    }
    
    public String read() {
        return "basic level of pupil reading";
    }
    
    public String write() {
        return "basic level of pupil writing";
    }
    
    public String relax() {
        return "basic level of pupil relaxing";
    }
}
