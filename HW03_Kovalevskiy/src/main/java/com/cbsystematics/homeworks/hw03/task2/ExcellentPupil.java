package com.cbsystematics.homeworks.hw03.task2;

/**
 * ExcellentPupil Class is an inheritor of Pupil class.
 * Has overridden methods that outputs a String information of current Pupil.
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class ExcellentPupil extends Pupil {

    public ExcellentPupil(String pupilName) {
        super(pupilName);
    }
    
    @Override
    public String study() {
        return "excellent level of pupil achievement";
    }
    
    @Override
    public String read() {
        return "excellent level of pupil reading";
    }
    
    @Override
    public String write() {
        return "excellent level of pupil writing";
    }
    
    @Override
    public String relax() {
        return "excellent level of pupil relaxing";
    }
}
