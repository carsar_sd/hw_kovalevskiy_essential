package com.cbsystematics.homeworks.hw03.task3;

/**
 * Ship Class is an inheritor of Vehicle class.
 * Describes Ship object characteristics
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Ship extends Vehicle {
    protected String port;

    public Ship(String coords, int price, int speed, int year, String port) {
        super(coords, price, speed, year);
        this.port = port;
    }

    /**
     * The method print information about the Ship
     */
    @Override
    public void printInformation() {
        System.out.println(
                "Ship  \uD83D\uDEA2 [ " +
                "coords: " + this.coords +
                ", price: " + String.format("%,d", this.price).replace(",", " ") + " USD" +
                ", speed: " + this.speed + " km/h" +
                ", year: " + this.year +
                ", port: " + this.port +
                " ]"
        );
    }
}
