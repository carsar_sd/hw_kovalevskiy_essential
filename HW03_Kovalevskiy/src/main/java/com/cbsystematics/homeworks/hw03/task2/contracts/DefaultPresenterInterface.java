package com.cbsystematics.homeworks.hw03.task2.contracts;

public interface DefaultPresenterInterface {
    String[] presentCollection(Object[] items);
}
