package com.cbsystematics.homeworks.hw03.task2;

import com.cbsystematics.homeworks.hw03.task2.presenters.PupilPresenter;
import com.cbsystematics.homeworks.hw03.utils.ConsoleColors;

import java.util.Arrays;

/**
 * ClassRoom Class takes class number and pupils as parameters
 * and print information about itself
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class ClassRoom {
    
    public static final int MAX_CLASS_SIZE = 4;
    
    private final int classNumber;
    private final PupilPresenter presenter;
    private final Pupil[] pupilsList;
    private final Pupil[] pupilsRest;

    public ClassRoom(int classNumber, PupilPresenter presenter, Pupil ...pupils) {
        this.classNumber = classNumber;
        this.presenter = presenter;
        this.pupilsList = pupils.length > ClassRoom.MAX_CLASS_SIZE
                ? Arrays.copyOfRange(pupils, 0, ClassRoom.MAX_CLASS_SIZE)
                : pupils;
        this.pupilsRest = pupils.length > ClassRoom.MAX_CLASS_SIZE
                ? Arrays.copyOfRange(pupils, ClassRoom.MAX_CLASS_SIZE, pupils.length)
                : null;
    }
    
    /**
     * The method print information about ClassRoom and pupils
     */
    public void printClassInfo() {
        System.out.println("\nClassRoom: #" + this.classNumber);
        System.out.println("Composition of pupils:");
    
        if (this.pupilsList.length > 0) {
            Arrays.asList(this.presenter.presentCollection(this.pupilsList)).forEach(System.out::println);
            System.out.println(ConsoleColors.YELLOW_BOLD_BRIGHT + "Classroom is full!\n" + ConsoleColors.RESET);
        } else {
            System.out.println(ConsoleColors.YELLOW_BOLD_BRIGHT + "Classroom is empty!\n" + ConsoleColors.RESET);
        }
    
        if (this.pupilsRest != null) {
            System.out.println("Rest of pupils:");
            Arrays.asList(this.presenter.presentCollection(this.pupilsRest)).forEach(System.out::println);
            System.out.println(ConsoleColors.YELLOW_BOLD_BRIGHT + "Use other classrooms to host them!\n" + ConsoleColors.RESET);
        }
    }
}
