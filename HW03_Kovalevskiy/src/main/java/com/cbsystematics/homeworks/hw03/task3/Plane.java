package com.cbsystematics.homeworks.hw03.task3;

/**
 * Plane Class is an inheritor of Vehicle class.
 * Describes Plane object characteristics
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Plane extends Vehicle {
    protected int height;
    protected int passengers;

    public Plane(String coords, int price, int speed, int year, int height, int passengers) {
        super(coords, price, speed, year);
        this.height = height;
        this.passengers = passengers;
    }

    /**
     * The method print information about the Plane
     */
    @Override
    public void printInformation() {
        System.out.println(
                "Plane \u2708 [ " +
                "coords: " + this.coords +
                ", price: " + String.format("%,d", this.price).replace(",", " ") + " USD" +
                ", speed: " + this.speed + " km/h" +
                ", year: " + this.year +
                ", height: " + this.height + " m" +
                ", passengers: " + this.passengers +
                " ]"
        );
    }
}
