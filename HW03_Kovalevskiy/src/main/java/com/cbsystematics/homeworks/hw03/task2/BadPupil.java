package com.cbsystematics.homeworks.hw03.task2;

/**
 * BadPupil Class is an inheritor of Pupil class.
 * Has overridden methods that outputs a String information of current Pupil.
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class BadPupil extends Pupil {
    
    public BadPupil(String pupilName) {
        super(pupilName);
    }
    
    @Override
    public String study() {
        return "bad level of pupil achievement";
    }
    
    @Override
    public String read() {
        return "bad level of pupil reading";
    }
    
    @Override
    public String write() {
        return "bad level of pupil writing";
    }
    
    @Override
    public String relax() {
        return "bad level of pupil relaxing";
    }
}
