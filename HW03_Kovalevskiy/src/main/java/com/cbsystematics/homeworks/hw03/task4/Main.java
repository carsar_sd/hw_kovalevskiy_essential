package com.cbsystematics.homeworks.hw03.task4;

import com.cbsystematics.homeworks.hw03.task4.contracts.Worker;
import com.cbsystematics.homeworks.hw03.task4.factories.WorkerFactory;
import com.cbsystematics.homeworks.hw03.task4.helpers.ScannerHelper;

import java.util.Scanner;

/**
 * Main Class is a main class that shows the result of using DocumentWorker classes
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Main {

    public static void main(String[] args) {
        String version = Main.getWorkerVersion();
        WorkerFactory workerFactory = new WorkerFactory();
        Worker worker = workerFactory.getWorker(version);
    
        System.out.println("\nYour possibilities:");
        worker.openDocument();
        worker.editDocument();
        worker.saveDocument();
        System.out.println("That's it.\n");
    }
    
    /**
     * The method takes the input from the console and return worker version
     */
    public static String getWorkerVersion() {
        System.out.println("\n-> Hello! This is a version chooser.");
        System.out.println("-> Please enter the key (\"pro\" or \"exp\") to work with paid version");
        System.out.println("-> or press \"Enter\" to work with free version");
        System.out.println("-> If you want to exit this program - type \"exit\" !\n");
        
        Scanner in = new Scanner(System.in);
        
        String version = Main.getConsoleStringData(in);
        
        in.close();
        
        return version;
    }
    
    /**
     * The method takes, validates and returns input from the console
     *
     * @param in Scanner instance
     * @return string console data
     */
    public static String getConsoleStringData(Scanner in) {
        boolean isCorrectString;
        String string;
        
        do {
            System.out.print("-> Enter a version key: ");
    
            string = in.nextLine().trim();
            
            if (string.isEmpty()) {
                return "free";
            }
            
            isCorrectString = string.matches("(pro|exp)");

            if (isCorrectString) {
                System.out.println("-> OK! ");
            } else {
                if (string.equals("exit")) {
                    ScannerHelper.runExitFull(in);
                }
                
                System.out.println("-> NOT OK! Your key is incorrect!");
            }
        } while (!isCorrectString);
        
        return string;
    }
}

/*
 - Всю логику(из данного файла), что не находится в методе main лучше выносить в сервисы?
   Или допустимо, с точки зренения архитектуры, наличие логики в "главном" файле?
*/