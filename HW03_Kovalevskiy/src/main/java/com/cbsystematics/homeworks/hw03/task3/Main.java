package com.cbsystematics.homeworks.hw03.task3;

/**
 * Main Class is a main class that shows the result of using Vehicle classes
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Main {

    public static void main(String[] args) {
        Vehicle plane = new Plane(
                "41°24'12.2\"N 2°10'26.5\"E.", 170000000, 1200, 2015, 18, 280
        );
        plane.printInformation();

        Vehicle ship = new Ship(
                "41°24'12.2\"N 2°10'26.5\"E.", 70000000, 1200, 2015, "Odessa Central Port"
        );
        ship.printInformation();

        Vehicle car = new Car(
                "41°24'12.2\"N 2°10'26.5\"E.", 20000, 1200, 2015
        );
        car.printInformation();
    }
}
