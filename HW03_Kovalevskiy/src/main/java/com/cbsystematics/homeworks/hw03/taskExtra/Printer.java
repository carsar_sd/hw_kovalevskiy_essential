package com.cbsystematics.homeworks.hw03.taskExtra;

/**
 * Printer Class is a parent of the color printer class group.
 * Has one basic print method that outputs a String parameter.
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Printer {

    /**
     * The method print input String parameter
     */
    public void print(String value) {
        System.out.println(value);
    }
}
