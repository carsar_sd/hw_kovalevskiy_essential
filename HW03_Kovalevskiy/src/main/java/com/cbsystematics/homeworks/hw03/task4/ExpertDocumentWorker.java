package com.cbsystematics.homeworks.hw03.task4;

/**
 * ExpertDocumentWorker Class is an inheritor of FreeDocumentWorker class.
 * Describes ExpertDocumentWorker methods
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class ExpertDocumentWorker extends ProDocumentWorker {
    
    /**
     * The method print information about saving possibility
     */
    @Override
    public void saveDocument() {
        System.out.println(" - The document is saved in the new format.");
    }
}
