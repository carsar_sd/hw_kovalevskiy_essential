package com.cbsystematics.homeworks.hw03.taskExtra;

import com.cbsystematics.homeworks.hw03.utils.ConsoleColors;

/**
 * PrinterBlue Class is an inheritor of Printer class.
 * Has one overridden print method that outputs a String parameter in red.
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class PrinterRed extends Printer {

    /**
     * The method print input String parameter in red.
     */
    @Override
    public void print(String value) {
        System.out.println(ConsoleColors.RED + value + ConsoleColors.RESET);
    }
}
