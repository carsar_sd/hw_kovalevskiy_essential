package com.cbsystematics.homeworks.hw03.task4;

import com.cbsystematics.homeworks.hw03.task4.contracts.Worker;
import com.cbsystematics.homeworks.hw03.utils.ConsoleColors;

/**
 * FreeDocumentWorker Class is a parent of the DocumentWorker class group.
 * Describes basic methods
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class FreeDocumentWorker implements Worker {
    
    /**
     * The method print information about reading possibility
     */
    @Override
    public void openDocument() {
        System.out.println(" - The document is open.");
    }
    
    /**
     * The method print information about writing possibility
     */
    @Override
    public void editDocument() {
        System.out.println(
                ConsoleColors.YELLOW_BRIGHT +
                " - Document editing is available in the Pro version." +
                ConsoleColors.RESET
        );
    }
    
    /**
     * The method print information about saving possibility
     */
    @Override
    public void saveDocument() {
        System.out.println(
                ConsoleColors.YELLOW_BRIGHT +
                " - Document saving is available in the Pro version." +
                ConsoleColors.RESET
        );
    }
}
