package com.cbsystematics.homeworks.hw03.task2.presenters;

import com.cbsystematics.homeworks.hw03.task2.contracts.DefaultPresenterInterface;
import com.cbsystematics.homeworks.hw03.task2.Pupil;
import com.cbsystematics.homeworks.hw03.utils.ConsoleColors;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public final class PupilPresenter implements DefaultPresenterInterface {
    
    public String present(Pupil pupil, int index) {
        return String.format(" %d. %-15s: %-36s | %-32s | %-32s | %-33s |",
                index + 1,
                ConsoleColors.BLUE_BOLD_BRIGHT + pupil.getPupilName() + ConsoleColors.RESET,
                pupil.study(),
                pupil.read(),
                pupil.write(),
                pupil.relax()
        );
    }
    
    @Override
    public String[] presentCollection(Object[] items) {
        AtomicInteger index = new AtomicInteger();
        
        return Stream.of(items).map(item -> this.present((Pupil) item, index.getAndIncrement())).toArray(String[]::new);
    }
}
