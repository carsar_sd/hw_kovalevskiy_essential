package com.cbsystematics.homeworks.hw03.task4.factories;

import com.cbsystematics.homeworks.hw03.task4.FreeDocumentWorker;
import com.cbsystematics.homeworks.hw03.task4.ExpertDocumentWorker;
import com.cbsystematics.homeworks.hw03.task4.ProDocumentWorker;
import com.cbsystematics.homeworks.hw03.task4.contracts.Worker;

public class WorkerFactory {
    
    /**
     * The method creates an instance of the Worker class depending on the version
     *
     * @param version of the Worker
     * @return Worker instance
     */
    public Worker getWorker(String version) {
        switch (version) {
            case "pro":
                return new ProDocumentWorker();
            case "exp":
                return new ExpertDocumentWorker();
            default:
                return new FreeDocumentWorker();
        }
    }
}
