package com.cbsystematics.homeworks.hw03.task4;

import com.cbsystematics.homeworks.hw03.utils.ConsoleColors;

/**
 * ProDocumentWorker Class is an inheritor of FreeDocumentWorker class.
 * Describes ProDocumentWorker methods
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class ProDocumentWorker extends FreeDocumentWorker {
    
    /**
     * The method print information about writing possibility
     */
    @Override
    public void editDocument() {
        System.out.println(" - The document edited.");
    }
    
    /**
     * The method print information about saving possibility
     */
    @Override
    public void saveDocument() {
        System.out.println(
                ConsoleColors.YELLOW_BRIGHT +
                " - The document is saved in the old format, saving in other formats is available in the Expert version." +
                ConsoleColors.RESET
        );
    }
}
