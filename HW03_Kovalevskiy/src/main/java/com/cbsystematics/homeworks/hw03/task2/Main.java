package com.cbsystematics.homeworks.hw03.task2;

import com.cbsystematics.homeworks.hw03.task2.presenters.PupilPresenter;

/**
 * Main Class is a main class that shows the result of using ClassRoom and Pupil classes
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Main {

    public static void main(String[] args) {
        ClassRoom classRoom = new ClassRoom(
                24,
                new PupilPresenter(),
                new GoodPupil("Ann"),
                new ExcellentPupil("Mary"),
                new BadPupil("Mark"),
                new ExcellentPupil("Ally"),
                new ExcellentPupil("Bob"),
                new GoodPupil("Serj")
        );
    
        classRoom.printClassInfo();
    }
}

/*
 - немного отошел от условий задания, но вроде результат получился как заказывали
*/