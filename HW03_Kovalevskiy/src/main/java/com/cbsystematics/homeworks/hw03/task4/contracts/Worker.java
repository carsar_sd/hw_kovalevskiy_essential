package com.cbsystematics.homeworks.hw03.task4.contracts;

public interface Worker {
    void openDocument();
    void editDocument();
    void saveDocument();
}
