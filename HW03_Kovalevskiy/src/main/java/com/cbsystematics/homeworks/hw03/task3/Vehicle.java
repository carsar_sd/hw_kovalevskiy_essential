package com.cbsystematics.homeworks.hw03.task3;

/**
 * Vehicle Class is a parent of the vehicle class group.
 * Describes basic Vehicle object characteristics
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Vehicle {

    protected String coords;
    protected int price;
    protected int speed;
    protected int year;

    public Vehicle(String coords, int price, int speed, int year) {
        this.coords = coords;
        this.price = price;
        this.speed = speed;
        this.year = year;
    }

    /**
     * The method print information about the Vehicle
     */
    public void printInformation() {
        System.out.println(
                "Vehicle [ " +
                "coords: " + this.coords +
                ", price: " + String.format("%,d", this.price).replace(",", " ") + " USD" +
                ", speed: " + this.speed + " km/h" +
                ", year: " + this.year +
                " ]"
        );
    }
}
