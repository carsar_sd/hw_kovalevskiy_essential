package com.cbsystematics.homeworks.hw02.taskExtra;

public class Main {
    
    public static void main(String[] args) {
        float radius = 12.4f;
        
        System.out.printf("Circle area: %.2f\n", Main.MyArea.areaOfCircle(radius));
    }
    
    private static class MyArea {
        
        private static final double PI = 3.14;
        
        public static double areaOfCircle(float radius) {
            return MyArea.PI * Math.pow(radius, 2);
        }
    }
}
