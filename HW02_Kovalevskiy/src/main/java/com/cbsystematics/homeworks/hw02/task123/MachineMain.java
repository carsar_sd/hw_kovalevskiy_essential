package com.cbsystematics.homeworks.hw02.task123;

/**
 * Задание2 и Задание3 и Задание4
 */
public class MachineMain {
    
    public static void main(String[] args) {
        Machine machine1 = new MachineMain.Machine();
        System.out.println(machine1.toString());
        
        Machine machine2 = new MachineMain.Machine(2000);
        System.out.println(machine2.toString());
        
        Machine machine3 = new MachineMain.Machine(2017, "red");
        System.out.println(machine3.toString());
    
        /* ------------------------------ */
    
        MachineWide machineWide1 = new MachineMain.MachineWide(50.0, 2700, "yellow", 2015);
        System.out.println(machineWide1.toString());
    
        MachineWide machineWide2 = new MachineMain.MachineWide(2015, 150.0, "blue");
        System.out.println(machineWide2.toString());
    }
    
    private static class Machine {
        
        private int year;
        private String color;
        
        public Machine() {
        
        }
    
        public Machine(int year) {
            this.year = year;
        }
    
        public Machine(int year, String color) {
            this.year = year;
            this.color = color;
        }
    
        @Override
        public String toString() {
            return "Machine [ " + "year: " + year + ", color: " + color + " ] ";
        }
    }
    
    /**
     * Я надеюсь все варианты перегруженных методов не нужны?
     *
     * */
    private static class MachineWide {
        
        private int year;
        private double speed;
        private int weight;
        private String color;
        
        public MachineWide() {
        
        }
        
        /* ------------------------------ */
        
        public MachineWide(int year) {
            this.year = year;
        }
    
        public MachineWide(double speed) {
            this.speed = speed;
        }
        
        /* ------------------------------ */
        
        public MachineWide(int year, double speed) {
            this(speed);
            this.year = year;
        }
    
        public MachineWide(double speed, int weight) {
            this(speed);
            this.weight = weight;
        }
    
        /* ------------------------------ */
        
        public MachineWide(int year, double speed, int weight) {
            this(speed, weight);
            this.year = year;
        }
    
        public MachineWide(int year, double speed, String color) {
            this(year, speed);
            this.color = color;
        }
    
        /* ------------------------------ */
    
        public MachineWide(int year, double speed, int weight, String color) {
            this(year, speed, weight);
            this.color = color;
        }
    
        public MachineWide(double speed, int weight, String color, int year) {
            this(year, speed, weight, color);
        }
    
        /* ------------------------------ */
    
        @Override
        public String toString() {
            return "MachineWide [ " + "year: " + year + ", speed: " + speed + ", weight: " + weight + ", color: " + color + " ] ";
        }
    }
}
