package com.cbsystematics.homeworks.hw04.task2;

/**
 * Main Class is a main class that shows the result of using different file handlers
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Main {

    public static void main(String[] args) {
        XMLHandler xmlHandler = new XMLHandler("file.xml");
        System.out.println(xmlHandler.work());
    
        TXTHandler txtHandler = new TXTHandler("file.txt");
        System.out.println(txtHandler.work());
    
        DOCHandler docHandler = new DOCHandler("file.doc");
        System.out.println(docHandler.work());
    }
}
