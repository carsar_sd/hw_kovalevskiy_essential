package com.cbsystematics.homeworks.hw04.task2;

import com.cbsystematics.homeworks.hw04.task2.contracts.AbstractHandler;

public class DOCHandler extends AbstractHandler {
    
    public DOCHandler(String fileName) {
        super(fileName);
    }
    
    @Override
    protected String create() {
        return "DOC file is created!";
    }
    
    @Override
    protected String open() {
        return "DOC file is opened!";
    }
    
    @Override
    protected String change() {
        return "DOC file is changed!";
    }
    
    @Override
    protected String save() {
        return "DOC file is saved!";
    }
    
    public String work() {
        return String.format("\nFile \"%s\":\n - %s\n - %s\n - %s\n - %s",
                this.getFileName(),
                this.create(),
                this.open(),
                this.change(),
                this.save()
        );
    }
}
