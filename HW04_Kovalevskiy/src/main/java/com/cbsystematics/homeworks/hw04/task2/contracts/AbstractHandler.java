package com.cbsystematics.homeworks.hw04.task2.contracts;

public abstract class AbstractHandler {
    
    private final String fileName;
    
    public AbstractHandler(String fileName) {
        this.fileName = fileName;
    }
    
    protected String getFileName() {
        return this.fileName;
    }
    
    protected abstract String create();
    protected abstract String open();
    protected abstract String change();
    protected abstract String save();
}
