package com.cbsystematics.homeworks.hw04.task3;

/**
 * Main Class is a main class that shows the result of using interfaces inheritance and "diamond problem"
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Main {

    public static void main(String[] args) {
        Player player = new Player("play");
        System.out.println(player.work());
    
        Player recorder = new Player("record");
        System.out.println(recorder.work());
    }
}
