package com.cbsystematics.homeworks.hw04.task2;

import com.cbsystematics.homeworks.hw04.task2.contracts.AbstractHandler;

public class XMLHandler extends AbstractHandler {
    
    public XMLHandler(String fileName) {
        super(fileName);
    }
    
    @Override
    protected String create() {
        return "XML file is created!";
    }
    
    @Override
    protected String open() {
        return "XML file is opened!";
    }
    
    @Override
    protected String change() {
        return "XML file is changed!";
    }
    
    @Override
    protected String save() {
        return "XML file is saved!";
    }
    
    public String work() {
        return String.format("\nFile \"%s\":\n - %s\n - %s\n - %s\n - %s",
                this.getFileName(),
                this.create(),
                this.open(),
                this.change(),
                this.save()
        );
    }
}
