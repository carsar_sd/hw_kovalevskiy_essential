package com.cbsystematics.homeworks.hw04.task3.contracts;

public interface Imitable {
    
    String pause();
    String stop();
}
