package com.cbsystematics.homeworks.hw04.task3.contracts;

public interface Recordable extends Imitable {
    
    String record();
    
    @Override
    default String pause() {
        return "Pausing recording!";
    }
    
    @Override
    default String stop() {
        return "Stop recording!";
    }
}
