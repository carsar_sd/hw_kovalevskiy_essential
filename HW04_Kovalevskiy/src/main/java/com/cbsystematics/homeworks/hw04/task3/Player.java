package com.cbsystematics.homeworks.hw04.task3;

import com.cbsystematics.homeworks.hw04.task3.contracts.Playable;
import com.cbsystematics.homeworks.hw04.task3.contracts.Recordable;

public class Player implements Playable, Recordable {
    
    private final String action;
    
    public Player(String action) {
        this.action = action;
    }
    
    @Override
    public String play() {
        return "Start playing..";
    }
    
    @Override
    public String record() {
        return "Start recording..";
    }
    
    @Override
    public String pause() {
        return (this.action.equals("play")) ? Playable.super.pause() : Recordable.super.pause();
    }
    
    @Override
    public String stop() {
        return (this.action.equals("play")) ? Playable.super.stop() : Recordable.super.stop();
    }
    
    public String work() {
        return String.format(" - %s\n - %s\n - %s\n",
                (this.action.equals("play")) ? this.play() : this.record(),
                this.pause(),
                this.stop()
        );
    }
}
