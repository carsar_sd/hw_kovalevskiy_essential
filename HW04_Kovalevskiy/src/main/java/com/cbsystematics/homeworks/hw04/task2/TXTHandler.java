package com.cbsystematics.homeworks.hw04.task2;

import com.cbsystematics.homeworks.hw04.task2.contracts.AbstractHandler;

public class TXTHandler extends AbstractHandler {
    
    public TXTHandler(String fileName) {
        super(fileName);
    }
    
    @Override
    protected String create() {
        return "TXT file is created!";
    }
    
    @Override
    protected String open() {
        return "TXT file is opened!";
    }
    
    @Override
    protected String change() {
        return "TXT file is changed!";
    }
    
    @Override
    protected String save() {
        return "TXT file is saved!";
    }
    
    public String work() {
        return String.format("\nFile \"%s\":\n - %s\n - %s\n - %s\n - %s",
                this.getFileName(),
                this.create(),
                this.open(),
                this.change(),
                this.save()
        );
    }
}
