package com.cbsystematics.homeworks.hw04.task3.contracts;

public interface Playable extends Imitable {
    
    String play();
    
    @Override
    default String pause() {
        return "Pausing playing!";
    }
    
    @Override
    default String stop() {
        return "Stop playing!";
    }
}
