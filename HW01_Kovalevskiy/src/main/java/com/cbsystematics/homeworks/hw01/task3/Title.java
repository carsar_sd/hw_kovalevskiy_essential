package com.cbsystematics.homeworks.hw01.task3;

import com.cbsystematics.homeworks.hw01.task3.contracts.DataInterface;

public class Title implements DataInterface {
    
    private final String title;
    
    public Title(String title) {
        this.title = title;
    }
    
    @Override
    public void show() {
        System.out.println("Title: " + this.title);
    }
}
