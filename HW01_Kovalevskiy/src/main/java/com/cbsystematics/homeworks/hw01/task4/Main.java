package com.cbsystematics.homeworks.hw01.task4;

import java.util.Arrays;

public class Main {
    
    public static void main(String[] args) {
        String[] models = {"Asus", "Apple", "HP", "Lenovo", "Samsung"};
        Object[] computers = new Object[5];
    
        for (int i = 0; i < computers.length; i++) {
            computers[i] = new Computer(models[i]);
        }
    
        Arrays.asList(computers).forEach(System.out::println);
    }
}
