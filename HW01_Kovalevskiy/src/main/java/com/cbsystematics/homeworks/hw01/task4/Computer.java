package com.cbsystematics.homeworks.hw01.task4;

public class Computer {
    
    public String model;
    
    public Computer(String model) {
        this.model = model;
    }
    
    @Override
    public String toString() {
        return "Computer [ model: " + model + " ]";
    }
}
