package com.cbsystematics.homeworks.hw01.taskExtra;

class Main {
    
    public static void main(String[] args) {
        Address addres1 = new Address(
                "03056", "Kiev", "Ukraine", "Borshahyvska", "146", "1"
        );
        System.out.println(addres1.toString());
        
        Address addres2 = new Address()
                .setIndex("03056")
                .setCountry("Kiev")
                .setCity("Ukraine")
                .setStreet("Borshahyvska")
                .setHouse("146")
                .setApartment("12");
        System.out.println(addres2.toString());
    }
}

/*
    Сеттеры немного не классического вида... так ... для разнообразия
*/