package com.cbsystematics.homeworks.hw01.task2;

public class Main {
    
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(12, 22);
        System.out.printf("Area: %.2f\nPerimeter: %.2f\n", rectangle.areaCalculator(), rectangle.perimeterCalculator());
        
        System.out.println("----------------");
        
        System.out.printf("Area: %.2f\nPerimeter: %.2f\n",
                Main.areaCalculator(rectangle.side1, rectangle.side2),
                Main.perimeterCalculator(rectangle.side1, rectangle.side2)
        );
    }
    
    public static double areaCalculator(double side1, double side2) {
        return side1 * side2;
    }
    
    public static double perimeterCalculator(double side1, double side2) {
        return 2 * (side1 + side2);
    }
}

/*
Очень странное задание: непонятно, методы нужно создать в классе или во вне?
Если в классе - зачем им параметры, если во вне - то зачем они в принципе?
Просто что-бы создать всего разного? Ну.. такое
*/