package com.cbsystematics.homeworks.hw01.task3;

import com.cbsystematics.homeworks.hw01.task3.contracts.DataInterface;

public class Author implements DataInterface {
    
    private final String author;
    
    public Author(String author) {
        this.author = author;
    }
    
    @Override
    public void show() {
        System.out.println("Author: " + this.author);
    }
}
