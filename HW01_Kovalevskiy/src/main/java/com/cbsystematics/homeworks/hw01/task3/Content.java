package com.cbsystematics.homeworks.hw01.task3;

import com.cbsystematics.homeworks.hw01.task3.contracts.DataInterface;

public class Content implements DataInterface {
    
    private final String content;
    
    public Content(String content) {
        this.content = content;
    }
    
    @Override
    public void show() {
        System.out.println("Content: " + this.content);
    }
}
