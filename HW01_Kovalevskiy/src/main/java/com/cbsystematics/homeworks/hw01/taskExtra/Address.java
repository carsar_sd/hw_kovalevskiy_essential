package com.cbsystematics.homeworks.hw01.taskExtra;

/**
 * Address Class describes object
 *
 * @author e.kovalevskiy
 * @version 1.0
 */
public class Address {
    
    private String index;
    private String country;
    private String city;
    private String street;
    private String house;
    private String apartment;
    
    public Address() {

    }
    
    public Address(String index, String country, String city, String street, String house, String apartment) {
        this.index = index;
        this.country = country;
        this.city = city;
        this.street = street;
        this.house = house;
        this.apartment = apartment;
    }
    
    public Address setIndex(String index) {
        this.index = index;
        return this;
    }
    
    public Address setCountry(String country) {
        this.country = country;
        return this;
    }
    
    public Address setCity(String city) {
        this.city = city;
        return this;
    }
    
    public Address setStreet(String street) {
        this.street = street;
        return this;
    }
    
    public Address setHouse(String house) {
        this.house = house;
        return this;
    }
    
    public Address setApartment(String apartment) {
        this.apartment = apartment;
        return this;
    }
    
    @Override
    public String toString() {
        return "Address [ " +
                "index: " + index +
                ", country: " + country +
                ", city: " + city +
                ", street: " + street +
                ", house: " + house +
                ", apartment: " + apartment +
                " ]";
    }
}
