package com.cbsystematics.homeworks.hw01.task2;

public class Rectangle {
    
    double side1, side2;
    
    public Rectangle(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
    }
    
    public double areaCalculator() {
        return this.side1 * this.side2;
    }
    
    public double perimeterCalculator() {
        return 2 * (this.side1 + this.side2);
    }
}
