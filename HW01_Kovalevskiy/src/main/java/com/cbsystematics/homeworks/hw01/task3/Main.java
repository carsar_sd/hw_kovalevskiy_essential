/*
    Очень непонятное задание. Ни условие ни смысл.
*/
package com.cbsystematics.homeworks.hw01.task3;

import com.cbsystematics.homeworks.hw01.task3.contracts.DataInterface;

public class Main {
    
    public static void main(String[] args) {
        DataInterface title = new Title("title");
        title.show();
        
        DataInterface author = new Author("author");
        author.show();
        
        DataInterface content = new Content("content");
        content.show();
    
        DataInterface book = new Book("title", "author", "content");
        book.show();
    }
}
