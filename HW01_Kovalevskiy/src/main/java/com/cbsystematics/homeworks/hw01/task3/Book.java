package com.cbsystematics.homeworks.hw01.task3;

import com.cbsystematics.homeworks.hw01.task3.contracts.DataInterface;

public class Book implements DataInterface {
    
    private final String title;
    private final String author;
    private final String content;
    
    public Book(String title, String author, String content) {
        this.title = title;
        this.author = author;
        this.content = content;
    }
    
    @Override
    public void show() {
        System.out.println(
                "Book [ " + "Title: " + title + ", Author: " + author + ", Content: " + content + " ]"
        );
    }
}
